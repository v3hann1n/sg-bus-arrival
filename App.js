/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import { Provider } from 'mobx-react'
import AppNavigator from './app/app.navigator'
import stores from './app/stores'
import {
	StyleProvider
} from 'native-base'
import getTheme from './native-base-theme/components'
import custom from './native-base-theme/variables/custom'

// const Realm = require('realm')
// import Realm from 'realm'

type Props = {};
export default class App extends Component<Props> {
	// componentWillMount() {
		// Realm.open({
		// 	schema: [{name: 'bookmarks', properties: {busStopCode: 'string', serviceNumber: 'string', }}]
		// }).then(realm => {
		// 	realm.create()
		// })
	// }

	render() {
		return (
			<Provider stores={stores}>
				<StyleProvider style={getTheme(custom)} >
					<AppNavigator />
				</StyleProvider>
			</Provider>
		);
	}
}
