import { observable, action } from 'mobx'
import Service from '../models/service.model'
import { LTA } from '../configs/lta.config'

export default class BusArrivalStore {
   @observable filter = ""
   @observable services = []
   @observable fetching = false
	@observable error = null
	
	constructor() {
		console.log("===========\n===========\n===============\nBusArrivalStore constructor")
	}

   @action
   searchArrival({serviceNumber, busStopCode}) {
      this.fetching = true

      console.log(busStopCode)

      let params = "?"
               + (busStopCode ? "BusStopCode=" + busStopCode : "")
               + (serviceNumber ? "&ServiceNumber=" + serviceNumber : "")
      console.log(LTA.urlBusArrival + params)
      fetch(LTA.urlBusArrival + params, {
         headers: {
            // Accept: 'application/json',
            // 'Content-type': 'application/json',
            "AccountKey": LTA.accountKey
         },
      })
      .then((response) => response.json())
      .then((responseJson) => {
         const services = responseJson.Services

         let servicesArray = new Array()
         for(service of services) {
            let svs = new Service({busStopCode: responseJson.BusStopCode, ...service})
            servicesArray.push(svs)
         }

         this.services = servicesArray

         this.fetching = false
      })
      .catch((error) => {
         console.error(error)
         this.error = error

         this.fetching = false
      })

      // const busTest = new Bus(busStopCode)
      // this.buses = [busTest]

      return Promise.resolve(null)
   }
}
