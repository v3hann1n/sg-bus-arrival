import BusArrivalStore from './busArrival.store'
import BusStopStore from './busStop.store'
import Realm from 'realm'

const busArrival = new BusArrivalStore()
const busStop = new BusStopStore()

export default {
	busArrival,
	busStop
}
