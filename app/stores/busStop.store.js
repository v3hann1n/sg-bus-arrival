import { observable, action } from 'mobx'
import Service from '../models/service.model'
import BusStop, { schema } from '../models/busStop.model'
import { LTA } from '../configs/lta.config'

import BusStopDatabase from '../database/busStop.database'

import Realm from 'realm'

export default class BusStopStore {
	@observable fetching = false
	@observable error = null
   @observable stops = []

	constructor() {
		console.log("===========\n===========\n===============\nBusStopStore constructor")
	}

	getAllStops() {
		return new Promise((resolve, reject) => {
			let getStops = (skipValue) => {
				let params = "?$skip=" + skipValue
		
				if (!this.fetching && this.error == null) {
					this.fetching = true
					return fetch(LTA.urlBusStops + params, {
						headers: {
							"AccountKey": LTA.accountKey
						},
					})
					.then((response) => response.json())
					.then((responseJson) => {
						this.fetching = false
		
						console.log(responseJson)
						// this.skipValue = responseJson.length + 1
		
						const busStopsData = responseJson.value

						if (busStopsData.length > 0) {
							let busStops = []
							for (busStopData of busStopsData) {
								// console.log(stopData)
								// let busStop = new BusStop({...stopData})
								// busStop.save()
								busStops.push(new BusStop({...busStopData}))
							}
							
							BusStopDatabase.saveBusStops(busStops)
							.then(() => {
								getStops(skipValue + 500)
							})
							.catch(error => {
								console.log(error)
								reject(error)
							})
						} else {
							resolve("Complete")
						}
					})
					.catch((error) => {
						console.error(error)
		
						this.fetching = false
		
						reject(error)
						
						// return Promise.reject(error)
					})
				}
			}

			getStops(0)
		})
	}
}