import Realm from 'realm'
import BusStop from '../models/busStop.model'

export default class BusStopDatabase {
	static saveBusStops(busStops) {
		return new Promise((resolve, reject) => {
			Realm.open({ schema: [BusStop.schema] })
				.then(realm => {
					for (busStop of busStops) {
						realm.write(() => {
							let stop = realm.create(BusStop.schema.name, {
								code: busStop.code,
								roadName: busStop.roadName,
								description: busStop.description,
								latitude: busStop.latitude,
								longitude: busStop.longitude
							})
						})
					}
					resolve(null)
				})
				.catch(error => {
					console.log(error)
					reject(error)
				})
		})
	}

	static busStopForCode(code) {
		return new Promise((resolve, reject) => {
			Realm.open({ schema: [BusStop.schema] })
				.then(realm => {
					let busStop = realm.objectForPrimaryKey(BusStop.schema.name, code)
					resolve(busStop)
				})
				.catch(error => {
					console.log(error)
					reject(error)
				})
		})
	}
}