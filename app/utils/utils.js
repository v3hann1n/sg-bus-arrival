import {
   StyleSheet
} from 'react-native'
import { BusLoad } from './../models/bus.model'

class UtilityFunctions {
   getColor(serviceLoad, type = "background") {
		let styleObj = seatsAvailableStyles
		let colorText = '#63C3B7'

      switch (serviceLoad) {
			// case BusLoad.sea: { // seats available
				
         //    styleObj = seatsAvailableStyles
         //    break
         // }
         case BusLoad.sda: { // standing available
				styleObj = standingStyles
				colorText = '#F7D66C'
            break
         }
         case BusLoad.lsd: { // limited standing
            styleObj = standingLimitedStyles
				colorText = '#F27C6E'
            break
         }
         default:
            break
		}
		
		if (type == "background") {
			return styleObj.background
		} else if (type == "font") {
			return styleObj.font
		} else if (type == "progress") {
			return colorText
		}
	}

	getProgressParams(serviceLoad) {
		let progress = 0.33
      switch (serviceLoad) {
         case BusLoad.sda: { // standing available
				progress = 0.66
            break
         }
			case BusLoad.lsd: { // limited standing
				progress = 1
            break
         }
         default:
            break
		}

		return  {
			color: Utils.getColor(serviceLoad, "progress"),
			progress: progress
		}
	}
}

const seatsAvailableStyles = StyleSheet.create({
   background: {
      backgroundColor: '#63C3B7' // '#064C08'
	},
   font: {
      color: 'white' // '#B7F6F3'
	},
	progress: {
		color: 'green'
	}
})

const standingStyles = StyleSheet.create({
   background: {
      backgroundColor: '#F7D66C' // '#C5C806'
	},
   font: {
      color: 'white' // '#F5F6B7'
   },
	progress: {
		color: 'orange'
	}
})

const standingLimitedStyles = StyleSheet.create({
   background: {
      backgroundColor: '#F27C6E' // '#780E05'
	},
   font: {
      color: 'white' // '#F7A39C'
   },
	progress: {
		color: 'red'
	}
})

const styles = StyleSheet.create({
   seatsAvailable: {
      backgroundColor: '#63C3B7' // '#064C08'
   },
   standing: {
      backgroundColor: '#F7D66C' //'#C5C806'
   },
   standingLimited: {
      backgroundColor: '#F27C6E' // '#780E05'
   },
   seatsAvailableFont: {
      color: 'white' // '#B7F6F3'
   },
   standingFont: {
      color: 'white' // '#F5F6B7'
   },
   standingLimitedFont: {
      color: 'white' // '#F7A39C'
   }
})

const Utils = new UtilityFunctions()

export default Utils