import React from 'react'
import { DrawerNavigator } from 'react-navigation'

import ListingScreen from './screens/listing.screen'

const Listing = {
    screen: ListingScreen,
    navigationOptions: {
        
        header: null,
        drawerLabel: "Home"
    }
}

const RouteConfig = {
    initialRoute: 'Listing'
}

const AppNavigator = DrawerNavigator({
    Listing: Listing
}, RouteConfig)

export default AppNavigator
