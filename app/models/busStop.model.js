import Realm from 'realm'

export const schema = 'BusStop'

class BusStop {
	constructor({BusStopCode, RoadName, Description, Latitude, Longitude}) {
		this.code = BusStopCode
		if (RoadName) {
			this.roadName = RoadName
		} else {
			this.roadName = BusStopCode
		}

		if (Description) {
			this.description = Description
		} else {
			this.description = BusStopCode
		}

		this.latitude = Latitude
		this.longitude = Longitude
	}

	save() {
		Realm.open({ schema: [BusStop.schema] })
			.then(realm => {
				realm.write(() => {
					let stop = realm.create(schema, {
						code: this.code,
						roadName: this.roadName,
						description: this.description,
						latitude: this.latitude,
						longitude: this.longitude
					})
				})
			}).catch(error => {
				console.log(error)
			})
	}
}

BusStop.schema = {
	name: 'BusStop',
	primaryKey: 'code',
	properties: {
		code: 'string',
		roadName: 'string',
		description: 'string',
		latitude: { type: 'float', default: 0 },
		longitude: { type: 'float', default: 0 }
	}
}

export default BusStop