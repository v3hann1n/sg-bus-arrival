import TimeAgo from 'react-native-timeago'
import moment from 'moment'

export default class Bus {
   constructor({ busStopCode, ServiceNo, OriginCode, DestinationCode, EstimatedArrival, Latitude, Longitude, VisitNumber, Load, Feature, Type }) {
      this.busStopCode = busStopCode
      this.serviceNumber = ServiceNo
      this.originCode = OriginCode
      this.destinationCode = DestinationCode
      this.estimatedArrival = EstimatedArrival
      this.latitude = Latitude
      this.longitude = Longitude
      this.visitNumber = VisitNumber
      this.load = Load
      this.feature = Feature
      this.type = Type
   }
   
   get arrivalTime() {
      const now = moment(new Date())
      const arrival = moment(this.estimatedArrival)
      let diff = moment(arrival.diff(now))
      console.log(diff.format("m"))
      return diff > 60000 ? diff.format("m") : "Arr"
   }
}

export const BusLoad = {
   sea: "SEA",
   sda: "SDA",
   lsd: "LSD"
}