import Bus from '../models/bus.model'

export default class Service {
    constructor({ busStopCode, ServiceNo, Operator, NextBus, NextBus2, NextBus3 }) {
        console.log(busStopCode)
        this.busStopCode = busStopCode
        this.serviceNumber = ServiceNo
        this.operator = Operator
        this.busFirst = new Bus({ busStopCode, ServiceNo, ...NextBus })
        this.busSecond = new Bus({ busStopCode, ServiceNo, ...NextBus2 })
        this.busThird = new Bus({ busStopCode, ServiceNo, ...NextBus3 })
    }
}
