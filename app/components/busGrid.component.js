import React, { Component } from 'react';
import {
   Spinner,
   Text,
   Row,
   Col,
   Left
} from 'native-base'
import {
   StyleSheet
} from 'react-native'
import Grid from 'react-native-grid-component'

import BusCard from '../components/busCard.component'

import BusStopDatabase from '../database/busStop.database'
import BusStop from '../models/busStop.model';

export default class BusGrid extends Component {

   constructor(props) {
		super(props)
		
		this.state = {
			
		}
   }

   renderCard(service) {
      return <BusCard service={service} key={service.serviceNumber} />
   }

   render() {
      const { fetching, error, services } = this.props
      console.log(fetching)
      if (fetching) {
         return <Spinner />
      }

      if (error || services.length == 0) {
         return (
            <Text style={{ backgroundColor: "red" }}>
               {error ? error : "No Results"}
            </Text>
         )
		}
		
		let stopName = null
		BusStopDatabase.busStopForCode(services[0].busStopCode)
		.then(stop => {
			stopName = stop.description
			console.log(busStop.description)
		})

      return (
         <Col>
            <Row>
               <Left>
                  <Text style={{color: 'white'}}>{stopName ? stopName : "Loading"}</Text>
                  <Text note>Stop name</Text>
               </Left>
            </Row>
            <Row>
               <Grid
                  renderItem={(data, i) => this.renderCard(data)}
                  data={services}
                  itemsPerRow={2}
                  style={styles.grid}
               />
            </Row>
         </Col>
      )

      /*
      (
         <Grid>
            <Row>
               <Col>
                  <BusCard />
               </Col>
               <Col>
                  <BusCard />
               </Col>
            </Row>
         </Grid>
      )
      */
   }
}

const styles = StyleSheet.create({
   grid: {
      flex: 1
   },
});