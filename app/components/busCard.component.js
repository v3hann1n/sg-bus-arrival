import React, { Component } from 'react';
import {
	View,
	Animated,
	StyleSheet,
	TouchableOpacity
} from 'react-native'
import FrontBusCard from './front.busCard.component'
import BackBusCard from './back.busCard.component'

import { BusLoad } from './../models/bus.model'


export default class BusCard extends Component {

	componentWillMount() {
		this.animatedValue = new Animated.Value(0)
		this.value = 0
		this.animatedValue.addListener(({ value }) => {
			this.value = value
		})

		this.frontInterpolate = this.animatedValue.interpolate({
			inputRange: [0, 180],
			outputRange: [0, -185]
		})
		this.alphaInterpolate = this.animatedValue.interpolate({
			inputRange: [0, 180],
			outputRange: [180, 0]
		})
		this.backInterpolate = this.animatedValue.interpolate({
			inputRange: [0, 180],
			outputRange: ['180deg', '360deg']
		})
	}

	flipCard() {
		if (this.value >= 90) {
			Animated.spring(this.animatedValue, {
				toValue: 0,
				friction: 8,
				tension: 0
			}).start();
		} else {
			Animated.spring(this.animatedValue, {
				toValue: 180,
				friction: 8,
				tension: 0
			}).start();
		}
	}

	render() {
		const frontAnimatedStyle = {
			// transform: [
			//    { rotateX: this.frontInterpolate }
			// ]
			top: this.frontInterpolate,
			// opacity: this.alphaInterpolate
		}
		const backAnimatedStyle = {
			transform: [
				{ rotateX: this.backInterpolate }
			]
		}

		const { service, showStopInfo } = this.props
		// console.log(TimeAgo(new Date()))
		const key = service.busStopCode

		return (
			<TouchableOpacity style={{ flex: 1, overflow: 'hidden' }} onPress={() => this.flipCard()}>
				<Animated.View style={[styles.card, styles.flipCard, styles.flipCardBack, backAnimatedStyle]}>
					<BackBusCard {...this.props} />
				</Animated.View>
				<Animated.View style={[styles.card, styles.flipCard, frontAnimatedStyle]} >
					<FrontBusCard bus={service.busFirst} />
				</Animated.View>
			</TouchableOpacity>
		)
	}
}

const styles = StyleSheet.create({
	card: {
		flex: 1,
		width: "100%",
		aspectRatio: 1,
		margin: 1
	},
	flipCard: {
		backfaceVisibility: 'hidden'
	},
	flipCardBack: {
		position: "absolute",
		top: 0
	}
})