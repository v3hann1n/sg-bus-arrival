import React, { Component } from 'react';
import {
	View,
	StyleSheet
} from 'react-native'
import {
	Card,
	CardItem,
	Left,
	Right,
	Body,
	Text,
	Button,
	Icon
} from 'native-base'
import * as Progress from 'react-native-progress'

import Bus, { BusLoad } from './../models/bus.model'
import Utils from './../utils/utils'

// import Realm from 'realm'

export default class BackBusCard extends Component {

	renderCapacity(bus) {
		let display = null

		switch (bus.load) {
			case BusLoad.sea: { // seats available
				display = <Icon name="airline-seat-legroom-normal" style={styles.capacityIcon} />
				break
			}
			case BusLoad.sda: { // standing available
				display = <Icon name="accessibility" style={styles.capacityIcon} />
				break
			}
			case BusLoad.lsd: { // limited standing
				display = <Icon name="sentiment-dissatisfied" style={styles.capacityIcon} />
				break
			}
			default:
				break
		}
		return display
	}

	renderRow(bus) {
		if (bus == null) {
			return null
		}

		let progressParams = Utils.getProgressParams(bus.load)

		let busTypeSection = null
		if (bus.type != "SD") {
			busTypeSection = (
				<View style={{ alignItems: 'center' }}>
					<Text style={[{ color: progressParams.color }]}>{bus.type}</Text>
				</View>
			)
		}
		return (
			<View style={[styles.busRow]}>
				<View style={styles.busRowTextContainer}>
					<Text style={[styles.busRowText, { color: progressParams.color }]}>{bus.arrivalTime}</Text>
				</View>
				<View style={styles.busRowProgress}>
					<Progress.Bar progress={progressParams.progress} width={null} height={5} color={progressParams.color} />
					{busTypeSection}
				</View>
			</View>
		)
	}

	render() {
		const { service, showStopInfo } = this.props

		return (
			<View style={styles.backMain}>
				<View style={styles.body}>
					{this.renderRow(service.busFirst)}
					{this.renderRow(service.busSecond)}
					{this.renderRow(service.busThird)}
				</View>
				<View style={styles.header}>
					<Text style={styles.serviceNumber}>{service.serviceNumber}</Text>
				</View>
			</View>

		)
	}
}

const styles = StyleSheet.create({
	backMain: {
		backgroundColor: '#252627',
		flex: 1,
		flexDirection: 'column'
	},
	header: {
		flex: 1
	},
	body: {
		flex: 4,
		display: 'flex'
	},
	footer: {
		alignSelf: 'baseline',
		width: "100%",
		height: 40,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		display: 'flex',
		flex: 3,
		backgroundColor: 'red'
	},
	serviceNumber: {
		padding: 5,
		fontWeight: 'bold',
		color: 'white',
		fontSize: 18,
		flex: 1
	},
	busRow: {
		flex: 1,
		justifyContent: 'center', // vertical center
		flexDirection: 'row'
	},
	busRowTextContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		flex: 1
	},
	busRowText: {
		fontSize: 22,
		textAlign: 'right',
		alignSelf: 'stretch'
	},
	busRowProgress: {
		justifyContent: 'center',
		flex: 3,
		paddingLeft: 10,
		paddingRight: 10
	}


	,
	time: {
		flex: 2
	},
	capacity: {
		flex: 2,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center'
	},
	capacityIcon: {

	}
})