import React, { Component } from 'react';
import {
	Text,
	Icon
} from 'native-base'
import {
	View,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

import Bus, { BusLoad } from './../models/bus.model'
import Utils from './../utils/utils'

export default class FrontBusCard extends Component {

	alarm() {
		// this.preventDefault = true
		console.log("alarm")
	}

	bookmark() {
		console.log("bookmark")
	}

	render() {
		const { bus } = this.props

		return (
			<View style={[{ flex: 1, flexDirection: 'column' }, Utils.getColor(bus.load)]}>
				<View style={[styles.header]}>
					<TouchableOpacity onPress={this.bookmark.bind(this)}>
						<Icon name="bookmark-border" style={styles.bookmark} />
					</TouchableOpacity>
				</View>
				<View style={[styles.body, styles.serviceNumberContainer]}>
					<Text style={[styles.serviceNumberText, Utils.getColor(bus.load, "font")]}>{bus.serviceNumber}</Text>
				</View>
				<View style={[styles.footer]}>
					<TouchableOpacity style={{ flex: 1 }} onPress={this.alarm.bind(this)}>
						<Icon name="alarm-add" style={[styles.busAlarm]} />
					</TouchableOpacity>
					<View style={[styles.busTypeContainer]}>
						<Text style={[styles.busType]}>{bus.type}</Text>
					</View>
					<View style={[styles.busArrivalContainer]}>
						<Text style={[styles.busArrival]}>{bus.arrivalTime}</Text>
					</View>
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	header: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	body: {
		flex: 3
	},
	footer: {
		alignSelf: 'baseline',
		width: "100%",
		height: 40,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		display: 'flex'
	},
	bookmark: { 
		paddingRight: 5, 
		color: '#8a8a8a' 
	},
	serviceNumberContainer: {
		// position: 'absolute',
		// top: 0,
		// left: 0,
		// right: 0,
		// bottom: 0,
		justifyContent: 'center',
		alignItems: 'center'
	},
	serviceNumberText: {
		paddingBottom: "30%",
		fontWeight: 'bold',
		fontSize: 65
	},
	busAlarm: {
		alignSelf: 'flex-start',
		padding: 5,
		color: '#8a8a8a',
		fontSize: 30,
		justifyContent: 'center',
		alignItems: 'center',
	},
	busTypeContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		flex: 1,
	},
	busType: {
		color: '#8a8a8a',
		fontSize: 20,
		marginLeft: -10
	},
	busArrivalContainer: {
		justifyContent: 'center',
		alignItems: 'flex-end',
		flex: 1
	},
	busArrival: {
		color: '#585858',
		fontSize: 30,
		paddingRight: 10
	}
})