import React, { Component } from 'react';
import {
   Header,
   Item,
   Input,
   Icon,
   Text
} from 'native-base'

export default class Search extends Component {
   constructor(props) {
      super(props)
   }

   search(e) {
      const searchString = e.nativeEvent.text

      const { busArrival, busStop } = this.props.stores

      if (searchString.length > 4) {
         busArrival.searchArrival({busStopCode: searchString})
      } else if (searchString.length > 1) {
			// busStop.getAllStops().then(() => {
			// 	console.log("done")
			// })
         // TODO: search buses
      }
   }

   render() {
      return (
         <Header searchBar rounded>
            <Item>
               <Icon name="search" />
               <Input placeholder="Search"
                  onSubmitEditing={this.search.bind(this)}
                  returnKeyType="search"
                  value="09023"
               />
            </Item>
         </Header>
      )
   }
}
