
         <Card>
            {
               showStopInfo ?
                  <CardItem>
                     <Body>
                        <Text>{service.busStopCode}</Text>
                        <Text note>Stop name</Text>
                     </Body>
                  </CardItem>
                  : null
            }
            <CardItem cardBody style={styles.back}>
               <Body>
                  <Text style={styles.serviceNumber}>{service.serviceNumber}</Text>
               </Body>
            </CardItem>
            <CardItem style={styles.busRow}>
               <Left style={styles.time}>
                  <Text>{service.busFirst.arrivalTime}</Text>
               </Left>
               <Body style={styles.capacity}>
                  {this.renderCapacity(service.busFirst)}
               </Body>
               <Right>
                  <Button iconRight transparent>
                     <Icon name="more-vert" />
                  </Button>
               </Right>
            </CardItem>
            <CardItem style={{ backgroundColor: 'red', height: 50 }}>
               <Left style={styles.time}>
                  <Text>{service.busSecond.arrivalTime}</Text>
               </Left>
               <Body style={styles.capacity}>
                  {this.renderCapacity(service.busSecond)}
               </Body>
               <Right>
                  <Button iconRight transparent>
                     <Icon name="more-vert" />
                  </Button>
               </Right>
            </CardItem>
            <CardItem style={styles.busRow}>
               <Left style={styles.time}>
                  <Text>{service.busThird.arrivalTime}</Text>
               </Left>
               <Body style={styles.capacity}>
                  {this.renderCapacity(service.busThird)}
               </Body>
               <Right>
                  <Button iconRight transparent>
                     <Icon name="more-vert" />
                  </Button>
               </Right>
            </CardItem>
         </Card>