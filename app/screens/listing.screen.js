import React, { Component } from 'react';
import {
    Container,
    Content,
    Button,
    Text
} from 'native-base'

import Search from '../components/search.component'
import BusGrid from '../components/busGrid.component'

import { inject, observer } from 'mobx-react';

@inject("stores")
@observer
export default class ListingScreen extends Component {
    constructor(props) {
		  super(props)
		  
		  
    }

    render() {
		  const { busArrival, busStop } = this.props.stores
		  busStop.getAllStops()
		  
        console.log("buses " + busArrival.services)
        console.log("stops " + busStop.stops)

        return (
            <Container>
                <Search {...this.props} />
                <Content scrollEnabled={true} style={{ backgroundColor: "#252627" }}>
                    <BusGrid services={busArrival.services} fetching={busArrival.fetching} error={busArrival.error} />
                </Content>
            </Container>
        )
    }
}
